<?php get_header(); ?>
		
	<?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
		
		<div id="content post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<div id="representaciones">
				<h2><span>Nuestras</span> Representaciones<span>.</span></h2>
			</div>

			<div id="representaciones-logos">
			  <a class="cannes" href="https://www.canneslions.com/" target="_blank"></a>
			  <a class="effie" href="http://www.effie.cr/" target="_blank"></a>
			  <a class="volcan" href="http://festivalvolcan.cr/" target="_blank"></a>
			  <a class="shutterstock" href="https://www.facebook.com/Stockcr/" target="_blank"></a>
			</div>

											
		</div>

		<?php endwhile; ?>
		
	<?php else : ?>
		
		<?php get_template_part('inc/gone'); ?>
		
	<?php endif; ?>
		
<?php get_footer(); ?>