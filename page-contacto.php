<?php get_header(); ?>
		
	<?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
		
		<div id="content post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div id="contacto">
				<h2>Conversemos<span>.</span></h2>
				<div class="two-columns">
					<div>
						<p>Déjenos sus datos y nos pondremos<br>en
						contacto con usted.</p>
						<ul>
						 	<li><i class="fas fa-envelope"></i>direccion@comunidad.cr</li>
						 	<li><i class="fas fa-phone-volume"></i>(506) 6003-9875</li>
						</ul>
					</div>
						<?php echo do_shortcode( '[contact-form-7 id="35" title="Formulario de contacto"]' ); ?>
					</div>
				</div>						
			</div>

		<?php endwhile; ?>
		
	<?php else : ?>
		
		<?php get_template_part('inc/gone'); ?>
		
	<?php endif; ?>
		
<?php get_footer(); ?>