	<footer>

		<ul class="social">
			<li><a href="https://www.facebook.com/ComunidadY" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
			<li><a href="https://www.instagram.com/gentecomunidad/" target="_blank"><i class="fab fa-instagram"></i></a></li>
			<li><a href="https://www.youtube.com/channel/UChDrrplBuAAAYcT0Kkb2Oog" target="_blank"><i class="fab fa-youtube"></i></a></li>
			<li><a href="https://twitter.com/comunidadcr" target="_blank"><i class="fab fa-twitter"></i></a></li>
		</ul>

		<?php wp_nav_menu( array( 'theme_location' => 'menu-footer' ) ); ?>
		
		<p>SAN JOSÉ, COSTA RICA.&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;TEL: <span>506</span> 6003-9875&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Correo: direccion@comunidad.cr </p>

		<p><span>&copy; <?php echo date('Y') .' Comunidad. Todos los Derechos Reservados'; ?></span></p>
	</footer>
			
	<?php wp_footer(); ?>
	
	</body>
</html>