<?php get_header(); ?>
		
	<?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
		
		<div id="content post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div id="hero">
				<?php echo do_shortcode( '[smartslider3 slider=3]' ); ?>										
				<div class="title">
					<h1>Comunidad&nbsp;de Empresas&nbsp;de Comunicación<span>.</span></h1>
				</div>

				<a id="ver-video">
					<div>
						<p>SOMOS COMUNIDAD</p>
						<p>VER VIDEO</p>
					</div>
					<div>
						<i class="fas fa-play"></i>
					</div>
				</a>
			</div>

			<div id="video">
				<video controls>
				  <source src="<?php echo get_bloginfo('template_url') ?>/video/comunidad.mp4" type="video/mp4">
				</video>
				<a><i class="fas fa-times"></i></a>
			</div>



				<div id="comunidad">
					<h2>Comunidad<span>.</span></h2><p>Creemos en la creatividad y la promovemos como valor inspirador y detonante de resultados positivos para nuestros asociados, sus clientes y para el desarrollo de nuestras organizaciones.</p>			
				</div>

				<div id="mision">
					<h2>Misión<span>.</span></h2>
					<ul>
						<li>
							<div>
								<h3>Promover el&nbsp;valor</h3>
								<p>de la industria de la comunicación comercial</p>
							</div>	
						</li>
						<li>
							<div>
								<h3>Promover el&nbsp;desarrollo</h3>
								<p>profesional de sus miembros</p>
							</div>	
						</li>
						<li>
							<div>
								<h3>Proteger y&nbsp;fortalecer</h3>
								<p>las empresas del sector</p>
							</div>
						</li>
					</ul>
				</div>

				<div id="vyp">
					<div>
						<h2>Visión<span>.</span></h2>
						<p>Ser una agrupación empresarial relevante para la industria de la comunicación y para el país.</p>
					</div>
					<div>
						<h2>Próposito<span>.</span></h2>
						<p>Agregar valor a los asociados mediante la generación y consolidación de conocimiento y herramientas que fortalezcan su gestión en el día a día, proveniente de la integración de las mejores empresas y profesionales de la comunicación comercial.</p>
					</div>
				</div>

				<div id="visionarios">
					<div>
						<p>1958</p>
						<h2>Grupo de visionarios de la época<span>.</span></h2>
					</div>
					<div>
						<img src="<?php echo get_bloginfo('template_url') ?>/images/visionarios.jpg" alt="Grupo de visionarios de la época">
					</div>
				</div>

				<div id="historia">
					<h2>Nuestra historia</h2>
					<p>En el año 1958, un grupo de visionarios de la época identifican la necesidad de trabajar por el desarrollo de la industria de la comunicación a partir de un frente común. Estos visionarios fueron Alberto H. Garnier y sus hijos Arnaldo y Alberto Garnier Oreamuno, el señor Marco Antonio Gutiérrez Chamberlain, Tomás Aguilar Alvarado, Federico Aguilar Alvarado y Carlos Mangel.</p>
					<p>Es así como se funda la Asociación Nacional de Agencias de Publicidad (ANAP). A través de los años esta agrupación fue evolucionando y transformándose, como respuesta a la realidad del mercado. Fue así como en 1973 se convirtió en la Asociación Costarricense de Agencias de Publicidad (ASCAP) y que, finalmente de la mano de esta industria que se renueva constantemente, llegó en el año 2009 a transformarse en COMUNIDAD, con la intención de poder agrupar a todos los sectores que integran la industria de la Comunicacion Comercial, que son: agencias de publicidad, de relaciones públicas, de medios, digitales, las dedicadas a mercadeo móvil, diseño de sitios en Internet e interactivos, mercadeo directo y de bases de datos, BTL y shopper marketing, las cuáles comparten la idea de que la creatividad es la esencia misma de la comunicación y el punto de encuentro de todas las especialidad.</p>
				</div>

				<div id="linea">
					<ul>
						<li>1958</li>
						<li>1973</li>
						<li>2002</li>
						<li>2008</li>
						<li>2009</li>
						<li>2010</li>
						<li>2017</li>
					</ul>
					<?php echo do_shortcode( '[smartslider3 slider=4]' ); ?>
				</div>

				<div id="junta">
					<h2>Junta Directiva<span>.</span></h2>
					<ul>
						<li>
							<p>Presidente:</p>
							<h3>Mauricio Garnier</h3>
							<p>Garnier BBDO</p>
							<p><a href="https://www.linkedin.com/in/mauriciogarnier/" target="_blank"><i class="fab fa-linkedin-in"></i></a></p>
						</li>
						<li>
							<p>Vicepresidenta:</p>
							<h3>Carmen Mayela Fallas </h3>
							<p>Comunicación Corporativa</p>
							<p><a href="https://www.linkedin.com/in/carmen-mayela-fallas-2535776/" target="_blank"><i class="fab fa-linkedin-in"></i></a></p>
						</li>
						<li>
							<p>Tesorera:</p>
							<h3>Marlene Fernández Fernández</h3>
							<p>Interamericana de Comunicación</p>
							<p><a href="https://www.linkedin.com/in/marlene-fernandez-6b82b453/" target="_blank"><i class="fab fa-linkedin-in"></i></a></p>
						</li>
						<li>
							<p>Secretaria:</p>
							<h3>Marian Bákit Palacios</h3>
							<p>Ideas</p>
							<p><a href="https://www.linkedin.com/in/marian-b%C3%A1kit-6231683/" target="_blank"><i class="fab fa-linkedin-in"></i></a></p>
						</li>
						<li>
							<p>Vocal 1:</p>
							<h3>Ana Irene Esquivel Muñoz</h3>
							<p>4AM- Saatchi & Saatchi</p>
							<p><a href="https://www.linkedin.com/in/irene-esquivel-a3359243/" target="_blank"><i class="fab fa-linkedin-in"></i></a></p>
						</li>
						<li>
							<p>Vocal 2:</p>
							<h3>Douglas Castro Fernández</h3>
							<p>Leo Burnett</p>
							<p><a href="https://www.linkedin.com/in/douglas-castro-27ba8674/" target="_blank"><i class="fab fa-linkedin-in"></i></a></p>
						</li>
						<li>
							<p>Vocal 3:</p>
							<h3>Edson Brizuela Sibaja</h3>
							<p>Media Naranja</p>
							<p><a href="https://www.linkedin.com/in/edsonbrizuela/" target="_blank"><i class="fab fa-linkedin-in"></i></a></p>
						</li>
						<li>
							<p>Vocal 4:</p>
							<h3>Alan Saul Vainrub Beker</h3>
							<p>OMD</p>
							<p><a href="https://www.linkedin.com/in/alan-vainrub-073498/" target="_blank"><i class="fab fa-linkedin-in"></i></a></p>
						</li>
						<li>
							<p>Vocal 5:</p>
							<h3>Mauricio Artavia Cappella</h3>
							<p>Darwin Zone</p>
							<p><a href="https://www.linkedin.com/in/mauricio-artavia-cappella-91953b18/" target="_blank"><i class="fab fa-linkedin-in"></i></a></p>
						</li>
						<li>
							<p>Vocal 6:</p>
							<h3>Felipe Andrés Morice Fernández</h3>
							<p>Publimark Mullenlowe</p>
							<p><a href="https://www.linkedin.com/in/felipe-morice-a660742/" target="_blank"><i class="fab fa-linkedin-in"></i></a></p>
						</li>
						<li>
							<p>Vocal 7:</p>
							<h3>Wagner Cornejo Retana</h3>
							<p>Jotabequ</p>
							<p><a href="https://www.linkedin.com/in/wagner-cornejo-b259791/" target="_blank"><i class="fab fa-linkedin-in"></i></a></p>
						</li>
						<li>
							<p>Fiscal:</p>
							<h3>Manuel Gavilán</h3>
							<p><a href="https://www.linkedin.com/in/manuel-gavilan-99a54a16/" target="_blank"><i class="fab fa-linkedin-in"></i></a></p>
						</li>
					</ul>
				</div>

				<div id="etica">
					<div>
						<h2>Código<br>de ética<span>.</span></h2>
						<p><a href="<?php echo get_bloginfo('template_url') ?>/documentos/Código-de-Ética-Comunidad.pdf" target="_blank">Conocé más sobre nuestro código de ética aquí</a></p>
					</div>
					<div>
						<p>Todos los asociados nos regimos por un Código de ética, el cual busca promover, entre los Asociados, conductas acordes con la confianza mutua, la transparencia, la buena fé, y demás valores éticos y morales que promueve Comunidad, y que motivaron su conformación.</p>
						<p>Con este fin, se establecen las mejores prácticas y conductas de los Asociados entre sí y frente a terceros, sean estos clientes, proveedores, colaboradores, autoridades gubernamentales, o la comunidad en general.</p>
					</div>
				</div>

				<div id="mas">
					<div>
						<h3>Blog</h3>
						<p>Pasemos de las estrategias de retención a las de 'cultivo' del personal / Mauricio Garnier</p>
						<p><a href="<?php echo home_url( 'pasemos-de-las-estrategias-de-retencion-a-las-de-cultivo-del-personal/' ) ?>" target="_self">Ver más</a></p>
					</div>
					<div>
						<h3>Capacitaciones</h3>
						<p>Todos los meses la asociación realiza capacitaciones exclusivas para los colaboradores de nuestros asociados.</p>
						<p><a href="<?php echo home_url( '/category/capacitaciones/' ) ?>" target="_self">Ver más</a></p>
					</div>
				</div>
												
		</div>
		
		<?php endwhile; ?>
		
	<?php else : ?>
		
		<?php get_template_part('inc/gone'); ?>
		
	<?php endif; ?>
		
<?php get_footer(); ?>