<?php get_header(); ?>
	
	<?php if (have_posts()) : ?>

		<div id="hero-image">
			<h1>Capacitaciones<span>.</span></h1>
			<p>Próximas capacitaciones exclusivas para los colaboradores de nuestros asociados.</p>
		</div>

		<div id="capacitaciones">
		
			<?php while (have_posts()) : the_post(); ?>
			
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<h2><?php the_title(); ?> <span id="fecha"><?php echo get_post_meta($post->ID, 'fecha', true); ?></span></h2>
				
				
				<?php the_content(); ?>

				<a href="<?php echo get_post_meta($post->ID, 'link', true); ?>">Inscribirse</a>
				
			</div>
			
			<?php endwhile; ?>

		</div>
		
		<?php get_template_part('inc/more'); ?>
		
	<?php else : ?>
		
		<?php get_template_part('inc/gone'); ?>
		
	<?php endif; ?>
	
<?php get_footer(); ?>