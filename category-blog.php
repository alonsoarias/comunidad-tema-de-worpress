<?php get_header(); ?>
	
	<?php if (have_posts()) : ?>

		<div id="hero-image">
			<h1>Blog<span>.</span></h1>
			<p>Los mejores de la industria nos comparten su conocimiento.</p>
		</div>

		<div id="blog">
		
			<?php while (have_posts()) : the_post(); ?>
			
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<a href="<?php the_permalink(); ?>">

				<?php if (has_post_thumbnail()) the_post_thumbnail(); ?>
				
				<h2><?php the_title(); ?></h2>

				</a>
							
			</div>
			
			<?php endwhile; ?>

		</div>
		
		<?php get_template_part('inc/more'); ?>
		
	<?php else : ?>
		
		<?php get_template_part('inc/gone'); ?>
		
	<?php endif; ?>
	
<?php get_footer(); ?>