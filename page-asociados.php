<?php get_header(); ?>
		
	<?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
		
		<div id="content post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div id="asociados">
				<h2><span>Nuestros</span> Asociados<span>.</span></h2>
				<ul id="menu-asociados">
					<li class="todos active">Todos</li>
					<li class="publi inactive">Publicidad</li>
					<li class="rp inactive">Relaciones Públicas</li>
					<li class="digi inactive">Digitales</li>
					<li class="medios inactive">Centrales de Medios</li>
					<li class="btl inactive">BTL</li>
				</ul>
				<div id="asociados-logos">
					<a class="a360 publicidad" href="http://360integral.cr/" target="_blank"></a>
				  <a class="am publicidad" href="http://www.4amsaatchi.com/" target="_blank"></a>
				  <a class="arcade digital" href="http://arcader.life/ " target="_blank"></a>
				  <a class="avance publicidad" href="http://www.avancewwp.com/" target="_blank"></a>
				  <a class="brandy publicidad" href="http://www.brandy.la/ " target="_blank"></a>
				  <a class="busha publicidad" href="https://www.anagram.la/" target="_blank"></a>
				  <a class="camedia medios" href="http://www.camediacentral.com/ " target="_blank"></a>
				  <a class="comunicacion rp" href="http://cckcentroamerica.com/ " target="_blank"></a>
				  <a class="crea publicidad" href="http://www.fcb.com/location/san-jose " target="_blank"></a>
				  <a class="darwin digital" href="http://www.darwinzone.com/ " target="_blank"></a>
				  <a class="doubledigit digital" href="http://www.doubledigit.com/" target="_blank"></a>
				  <a class="fw btl" href="http://fw-marketing.com/ " target="_blank"></a>
				  <a class="gbbdo publicidad" href="http://www.garnierbbdo.com/ " target="_blank"></a>
				  <a class="hit btl" href="http://www.hitexp.com/ " target="_blank"></a>
				  <a class="house publicidad" href="http://www.houserapp.com/ " target="_blank"></a>
				  <a class="ideas publicidad" href="http://www.ideascr.com/ " target="_blank"></a>
				  <a class="interaction digital" href="http://www.interaction.cr/ " target="_blank"></a>
				  <a class="interamericana rp" href="http://www.interamericana.co.cr/ " target="_blank"></a>
				  <a class="jotabequ publicidad" href="http://www.jotabequ.com/ " target="_blank"></a>
				  <a class="jwt publicidad" href="https://www.jwt.com/costarica " target="_blank"></a>
				  <a class="kin btl" href="http://www.grafoskincom.cr/ " target="_blank"></a>
				  <a class="latres publicidad" href="http://www.latres.com/ " target="_blank"></a>
				  <a class="leo publicidad" href="http://leoburnettcr.com/" target="_blank"></a>
				  <a class="madison publicidad" href="http://www.madisonddb.com/#madison" target="_blank"></a>
				  <a class="mccann publicidad" href="http://mccann.com/" target="_blank"></a>
				  <a class="media btl" href="http://medianaranja.cr/ " target="_blank"></a>
				  <a class="ogilvy publicidad" href="https://www.facebook.com/OgilvyCR/ " target="_blank"></a>
				  <a class="omd medios" href="http://www.omd.com/latin-america/global-media-agency " target="_blank"></a>
				  <a class="osopez publicidad" href="http://www.osopez.com" target="_blank"></a>
				  <a class="pulse publicidad" href="www.pulse.works/" target="_blank"></a>
				  <a class="ninja publicidad" href="#" target="#"></a>
				  <a class="partner publicidad" href="http://www.partner.cr/ " target="_blank"></a>
				  <a class="porter rp" href="http://www.cacporternovelli.com/ " target="_blank"></a>
				  <a class="possible digital" href="https://www.possible.com/ " target="_blank"></a>
				  <a class="publimark publicidad" href="http://www.publimark.cr/ " target="_blank"></a>
				  <a class="republika digital" href="http://republikaindependiente.com/ " target="_blank"></a>
				  <a class="tac publicidad" href="https://www.tacagency.com/" target="_blank"></a>
				  <a class="tbwa publicidad" href="http://www.tbwacr.com/ " target="_blank"></a>
				  <a class="teah digital" href="http://www.teahdigital.com/ " target="_blank"></a>
				  <a class="tribu publicidad" href="http://havastribu.com/ " target="_blank"></a>
				  <a class="tres btl" href="http://tresdemarzo.com/en/producciones/producciones" target="_blank"></a>
				  <a class="wooki publicidad" href="http://wookicolectivo.com/ " target="_blank"></a>
				  <a class="yisus publicidad" href="#" target="_blank"></a>
				  <a class="yyr publicidad" href="#" target="_blank"></a>
				</div>
			</div>
											
		</div>

		<?php endwhile; ?>
		
	<?php else : ?>
		
		<?php get_template_part('inc/gone'); ?>
		
	<?php endif; ?>
		
<?php get_footer(); ?>