<div class="meta">
	<em><?php _e('Posted on'); ?></em> <time datetime="<?php the_time('Y-m-d'); ?>" pubdate><?php the_time('F j, Y'); ?></time>
</div>