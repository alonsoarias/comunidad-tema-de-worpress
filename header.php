<!DOCTYPE html> 
<html <?php language_attributes(); ?>>
	<head>
				
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
		<meta name="description" content="<?php if ( is_single() ) {
        single_post_title('', true); 
    } else {
        bloginfo('name'); echo " - "; bloginfo('description');
    }
    ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<?php wp_head(); ?>
		
	</head>
	
	<body <?php body_class(); ?>>
					
		<header>

			<div class="logo">
				<a href="<?php echo home_url( '/' ) ?>">
					<img src="<?php echo get_bloginfo('template_url') ?>/images/logotipo.jpg" alt="Comunidad de Empresas de Comunicación de Costa Rica">
				</a>
			</div>

			<div id="show-menu">
        <a><i class="fas fa-bars"></i></a>
        <a><i class="fas fa-times"></i></a>
      </div>

			<?php wp_nav_menu( array( 'theme_location' => 'menu-header' ) ); ?>

		</header>