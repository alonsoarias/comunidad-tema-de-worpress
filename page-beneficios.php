<?php get_header(); ?>
		
	<?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
		
		<div id="content post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div id="nuestros">
				<h2><span>Nuestros</span> Beneficios<span>.</span></h2>			
			</div>	

			<div id="bene">
				<p>Contamos con una serie de beneficios exclusivos para nuestros asociados.</p>
				<div id="beneficios">
					<ul>
						<li class="active legal">LEGAL</li>
						<li class="inactive docu">DOCUMENTOS</li>
						<li class="inactive capa">CAPACITACIONES</li>
						<li class="inactive bolsa">BOLSA DE EMPLEO</li>
					</ul>
					<div id="infobeneficios">
						<div class="active legal">
							<h3>LEGAL<span>.</span></h3>
							<h4>Asesoria gratuita para asociados</h4>
							<p>Comunidad cuenta con un acuerdo con la firma legal BLP, por medio de la cuál se brinda servicio de atención de consultas legales generales a nuestros asociados de forma gratuita.</p>
						</div>
						<div class="inactive docu">
							<h3>DOCUMENTOS<span>.</span></h3>
							<h4>Documentación relacionada con la industria</h4>
							<p>Ponemos a disposición de nuestros asociados investigaciones y documentación relevante para la industria.</p>
						</div>
						<div class="inactive capa">
							<h3>CAPACITACIONES<span>.</span></h3>
							<h4>Listado de talleres futuros</h4>
							<p>Todos los meses realizamos capacitaciones por medio de las cuáles los colaboradores de nuestros asociados se actualizan en todas las áreas.</p>
							<p><a href="<?php echo home_url( '/category/capacitaciones/' ) ?>" target="_self">Ver más</a></p>
						</div>	
						<div class="inactive bolsa">
							<h3>BOLSA DE EMPLEO<span>.</span></h3>
							<h4>¿Necesita colaboradoes? Nostros le ayudamos</h4>
							<p>Servicio de publicación de las ofertas de empleo de nuestros asociados en nuestra página web y en nuestras redes sociales.</p>
							<p><a href="<?php echo home_url( '/category/bolsa-de-empleo/' ) ?>" target="_self">Ver más</a></p>
						</div>		
					</div>
				</div>
			</div>
											
		</div>

		<?php endwhile; ?>
		
	<?php else : ?>
		
		<?php get_template_part('inc/gone'); ?>
		
	<?php endif; ?>
		
<?php get_footer(); ?>