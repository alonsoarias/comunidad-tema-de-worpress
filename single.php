<?php get_header(); ?>
	
	<div id="content">
	
	<?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>

		<div id="blog-post">
		
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<h1><?php the_title(); ?></h1>
							
				<?php if (has_post_thumbnail()) the_post_thumbnail(); ?>
				
				<div class="entry">
					
					<?php the_content(); ?>
									
				</div>
							
			</div>

		</div>
		
		<?php endwhile; ?>
		
	<?php else : ?>
		
		<?php get_template_part('inc/gone'); ?>
		
	<?php endif; ?>
	
	</div>	

<?php get_footer(); ?>