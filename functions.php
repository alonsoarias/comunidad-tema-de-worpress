<?php

if (!defined('ABSPATH')) exit;




function add_thumbnail() {

	add_theme_support( 'post-thumbnails', array( 'post' ) );

}
add_action('after_setup_theme', 'add_thumbnail');




function add_widgets() {
		
	register_sidebar( 'widgets' );
	
}
add_action('widgets_init', 'add_widgets');




function add_styles_and_scripts() {

	wp_enqueue_style( 'reboot', get_template_directory_uri() . '/css/reset.css' );

	wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.0.13/css/all.css' );
	
	wp_enqueue_style( 'main', get_template_directory_uri() . '/css/main.css', array(), date("h:i:s"));

	wp_enqueue_script('main', get_template_directory_uri() .'/js/main.js', array('jquery'), date("h:i:s"), true);
	
}
add_action('wp_enqueue_scripts', 'add_styles_and_scripts');




function add_menu() {
  register_nav_menus(
    array(
      'menu-header' => __( 'Header' ),
      'menu-footer' => __( 'Footer' )
    )
  );
}
add_action( 'init', 'add_menu' );