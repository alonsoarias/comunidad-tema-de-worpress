var $j = jQuery.noConflict();

$j( document ).ready(function() {
	slideNav();
	asociados();
	beneficios();
  video();
});


function slideNav() {
	$j("#show-menu a:first-child").click(function () {
		$j("#show-menu a:first-child").hide();
		$j("#show-menu a:nth-child(2)").show();
    $j("#menu-header").slideDown("slow");
	});
	$j("#show-menu a:nth-child(2)").click(function () {
		$j("#show-menu a:nth-child(2)").hide();
		$j("#show-menu a:first-child").show();
    $j("#menu-header").slideUp("slow");
	});
}


function asociados() {

  $j("#menu-asociados li").click(function(){
    if($j(this).hasClass("inactive")){ 
      $j("#menu-asociados li").removeClass("active");
      $j("#menu-asociados li").addClass("inactive");           
      $j(this).addClass("active");
      $j(this).removeClass("inactive");
    }
  });

  $j("#menu-asociados .todos").click(function () {
  	$j("#asociados-logos .publicidad").show(0);
  	$j("#asociados-logos .digital").show(0);
    $j("#asociados-logos .btl").show(0);
    $j("#asociados-logos .medios").show(0);
    $j("#asociados-logos .rp").show(0);
	});

	$j("#menu-asociados .publi").click(function () {
  	$j("#asociados-logos .publicidad").show(0);
  	$j("#asociados-logos .digital").hide(0);
    $j("#asociados-logos .btl").hide(0);
    $j("#asociados-logos .medios").hide(0);
    $j("#asociados-logos .rp").hide(0);
	});

	$j("#menu-asociados .rp").click(function () {
  	$j("#asociados-logos .publicidad").hide(0);
  	$j("#asociados-logos .digital").hide(0);
    $j("#asociados-logos .btl").hide(0);
    $j("#asociados-logos .medios").hide(0);
    $j("#asociados-logos .rp").show(0);
	});

	$j("#menu-asociados .digi").click(function () {
  	$j("#asociados-logos .publicidad").hide(0);
  	$j("#asociados-logos .digital").show(0);
    $j("#asociados-logos .btl").hide(0);
    $j("#asociados-logos .medios").hide(0);
    $j("#asociados-logos .rp").hide(0);
	});

	$j("#menu-asociados .medios").click(function () {
  	$j("#asociados-logos .publicidad").hide(0);
  	$j("#asociados-logos .digital").hide(0);
    $j("#asociados-logos .btl").hide(0);
    $j("#asociados-logos .medios").show(0);
    $j("#asociados-logos .rp").hide(0);
	});

	$j("#menu-asociados .btl").click(function () {
  	$j("#asociados-logos .publicidad").hide(0);
  	$j("#asociados-logos .digital").hide(0);
    $j("#asociados-logos .btl").show(0);
    $j("#asociados-logos .medios").hide(0);
    $j("#asociados-logos .rp").hide(0);
	});
}



function beneficios() {

	$j("#beneficios ul li").click(function(){
    if($j(this).hasClass("inactive")){ 
      $j("#beneficios ul li").removeClass("active");
      $j("#beneficios ul li").addClass("inactive");           
      $j(this).addClass("active");
      $j(this).removeClass("inactive");
    }
  });	

  $j("#beneficios ul .legal").click(function () {
  	$j("#infobeneficios .legal").show(0);
  	$j("#infobeneficios .docu").hide(0);
    $j("#infobeneficios .capa").hide(0);
    $j("#infobeneficios .bolsa").hide(0);
	});

	$j("#beneficios ul .docu").click(function () {
  	$j("#infobeneficios .legal").hide(0);
  	$j("#infobeneficios .docu").show(0);
    $j("#infobeneficios .capa").hide(0);
    $j("#infobeneficios .bolsa").hide(0);
	});

	$j("#beneficios ul .capa").click(function () {
  	$j("#infobeneficios .legal").hide(0);
  	$j("#infobeneficios .docu").hide(0);
    $j("#infobeneficios .capa").show(0);
    $j("#infobeneficios .bolsa").hide(0);
	});

	$j("#beneficios ul .bolsa").click(function () {
  	$j("#infobeneficios .legal").hide(0);
  	$j("#infobeneficios .docu").hide(0);
    $j("#infobeneficios .capa").hide(0);
    $j("#infobeneficios .bolsa").show(0);
	});

}


function video() {
  $j("#ver-video").click(function () {
    $j("#video").show();
    $j("#hero").hide();
    $j("video")[0].play();
  });

  $j("#video a").click(function () {
    $j("#video").hide();
    $j("#hero").show();
    $j("video")[0].pause();
  });
}