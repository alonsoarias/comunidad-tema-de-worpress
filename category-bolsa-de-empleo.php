<?php get_header(); ?>
	
	<?php if (have_posts()) : ?>

		<div id="hero-image">
			<h1>Bolsa de Empleo<span>.</span></h1>
		</div>

		<div id="bolsa">
		
			<?php while (have_posts()) : the_post(); ?>
			
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php echo get_the_post_thumbnail( $page->ID, 'thumbnail' ); ?>
				
				<h2><?php the_title(); ?></h2>
				<p id="empresa"><?php echo get_post_meta($post->ID, 'empresa', true); ?></p>
				
				<?php the_content(); ?>
									
			</div>
			
			<?php endwhile; ?>

		</div>
		
		<?php get_template_part('inc/more'); ?>
		
	<?php else : ?>
		
		<?php get_template_part('inc/gone'); ?>
		
	<?php endif; ?>
	
<?php get_footer(); ?>